mod client;
mod config;
mod server;
mod utils;

use anyhow::Error;
use clap::Parser;
use tracing_subscriber::EnvFilter;
use veilid_core::{CryptoKey, CryptoTyped, CryptoKind, CRYPTO_KIND_VLD0, FromStr, PublicKey, SecretKey};

use serde::{Serialize, Deserialize};

use std::fs::File;
use std::io::Write;
use std::env;
use std::path::Path;

const CRYPTO_KIND: CryptoKind = CRYPTO_KIND_VLD0;

/// Like netcat but with Veilid
#[derive(Parser, Debug)]
struct Args {
    #[arg(long)]
    server: bool,
    #[arg(long)]
    new_keys: bool,
    #[arg(long)]
    client: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct ServiceKeys {
   //pub key_pair: TypedKeyPair,
   pub public_key: PublicKey,
   pub secret_key: SecretKey,
   pub dht_key: Option<CryptoTyped::<CryptoKey>>,
   pub dht_owner_key: Option<PublicKey>,
   pub dht_owner_secret_key: Option<SecretKey>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    let default_env_filter = EnvFilter::try_from_default_env();
    let fallback_filter = EnvFilter::new("veilid_core=warn,info");
    let env_filter = default_env_filter.unwrap_or(fallback_filter);

    tracing_subscriber::fmt()
        .with_writer(std::io::stderr)
        .with_env_filter(env_filter)
        .init();

    let temp_dir = tempfile::tempdir()?;

    if args.server {
        let manifest_var = env::var("CARGO_MANIFEST_DIR")?;
        let manifest_path = Path::new(&manifest_var);
        let asset_path = manifest_path.join("assets");

        let service_keys;
        if args.new_keys {
            let key_pair = veilid_core::Crypto::generate_keypair(CRYPTO_KIND)?;
            service_keys = ServiceKeys {
                public_key: key_pair.value.key,
                secret_key: key_pair.value.secret,
                ..Default::default()
            };
            File::create(asset_path.join("secrets.json"))?.write_all(&serde_json::to_vec(&service_keys)?)?;
        } else {
            service_keys  = serde_json::from_reader(File::open(asset_path.join("secrets.json"))?)?;
        }
        server::server(temp_dir.into_path(), service_keys, args.new_keys).await?;
    } else if let Some(service_dht_str) = args.client {
        let service_dht_key = CryptoTyped::<CryptoKey>::from_str(&service_dht_str)?;
        client::client(temp_dir.into_path(), veilid_core::Crypto::generate_keypair(CRYPTO_KIND)?, service_dht_key).await?;
    } else {
        let service_dht_str = "VLD0:_rW0HDcOMxx_j8nLxfQ-3Zuu-VYkDfjR4xwTREV857Y";
        let service_dht_key = CryptoTyped::<CryptoKey>::from_str(service_dht_str)?;
        client::client(temp_dir.into_path(), veilid_core::Crypto::generate_keypair(CRYPTO_KIND)?, service_dht_key).await?;
    }

    Ok(())
}
