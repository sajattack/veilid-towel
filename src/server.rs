use anyhow::{Context, Error, anyhow};
use flume::{unbounded, Receiver, Sender};
use tracing::{info, debug, error};
use veilid_core::api_startup;
use veilid_core::tools::*;
use veilid_core::*;
use veilid_core::{SafetySpec, SafetySelection, Stability};

use crate::config::config_callback;
use crate::utils::{wait_for_attached, wait_for_network_start, wait_for_public_internet_ready};
use crate::CRYPTO_KIND;

use serde_json::{self, Value};

use std::path::Path;
use std::fs::File;
use std::io::{Read, Write};
use std::env;


pub async fn server(
    temp_dir: std::path::PathBuf,
    service_keys: crate::ServiceKeys,
    new_keys: bool,
) -> Result<(), Error> {

    // setup for towel
    let mut json_string = String::new();
    let manifest_var = env::var("CARGO_MANIFEST_DIR")?;
    let manifest_path = Path::new(&manifest_var);
    let asset_path = manifest_path.join("assets");
    File::open(asset_path.join("a_new_hope.json"))?.read_to_string(&mut json_string)?;
    let parsed: Value = serde_json::from_str(&json_string)?;
    let frames = parsed.as_array().ok_or(anyhow!("Failed to parse frames"))?;
    
    let temp_dir_clone = temp_dir.clone();

    let (sender, receiver): (
        Sender<veilid_core::VeilidUpdate>,
        Receiver<veilid_core::VeilidUpdate>,
    ) = unbounded();


    // Create VeilidCore setup
    let update_callback = Arc::new(move |change: veilid_core::VeilidUpdate| {
        if let Err(e) = sender.send(change) {
            // Don't log here, as that loops the update callback in some cases and will deadlock
            let change = e.into_inner();
            info!("error sending veilid update callback: {:?}", change);
        }
    });


    let api = api_startup(
        update_callback,
        Arc::new(move |key| config_callback(temp_dir_clone.clone(), CryptoTyped::new(CRYPTO_KIND, KeyPair::new(service_keys.public_key, service_keys.secret_key)), key)),
    )
    .await
    .expect("startup failed");

    api.attach().await?;

    wait_for_network_start(&api).await;

    wait_for_attached(&api).await;

    let rc = api.routing_context()?.with_safety(
        SafetySelection::Safe(
            SafetySpec { 
                preferred_route: None,
                hop_count: 1,
                stability: Stability::Reliable,
                sequencing: Sequencing::EnsureOrdered,
            }
        )
    )?;

    wait_for_public_internet_ready(&api).await?;

    info!("creating a private route");
    let (_route_id, blob) = api
        .new_custom_private_route(
            &[CRYPTO_KIND],
            veilid_core::Stability::Reliable,
            veilid_core::Sequencing::EnsureOrdered,
        )
        .await
        .context("new_custom_private_route")?;
    info!("creating a private route, done");


    use base64::{engine::general_purpose, Engine as _};

    let encoded: String = general_purpose::STANDARD_NO_PAD.encode(blob);

    info!("Publishing route on DHT: {}", encoded);
    if new_keys {
        match rc.create_dht_record(
            DHTSchema::SMPL(
                DHTSchemaSMPL{
                    o_cnt: 2,
                    members: vec![DHTSchemaSMPLMember{m_key:service_keys.public_key, m_cnt: 2}]
                }
            ), Some(CRYPTO_KIND)).await {
            Ok(rec) => { 
                info!("DHT Key: {}", *rec.key());
                match rc.set_dht_value(*rec.key(), 1, encoded.as_bytes().to_vec()).await {
                    Ok(_) => (),
                    Err(e) =>{error!("{}", e)},
                }
                rc.close_dht_record(*rec.key()).await?;
                let service_keys_new = crate::ServiceKeys {
                    public_key: service_keys.public_key,
                    secret_key: service_keys.secret_key,
                    dht_key: Some(*rec.key()), 
                    dht_owner_key: Some(*rec.owner()),
                    dht_owner_secret_key: rec.owner_secret().copied(),
                };
                info!("creating file");
                File::create(asset_path.join("secrets.json"))?.write_all(&serde_json::to_vec(&service_keys_new)?)?;
            },
            Err(e) => {error!("{}", e)},
        }
    } else {
        match service_keys.dht_key {
            Some(dht_key) => {
                info!("DHT Key: {}", dht_key);
                match (service_keys.dht_owner_key, service_keys.dht_owner_secret_key) {
                    (Some(public), Some(private)) =>
                    {

                        let key_pair = KeyPair::new(public, private);
                        match rc.open_dht_record(dht_key, Some(key_pair)).await {
                            Ok(rec) => { 
                                match rc.set_dht_value(*rec.key(), 1, encoded.as_bytes().to_vec()).await {
                                    Ok(_) => (),
                                    Err(e) =>{error!("{}", e)},
                                }
                                rc.close_dht_record(*rec.key()).await?;
                            },
                            Err(e) =>{error!("{}", e)},
                        }
                    },
                    _ => { error!("DHT owners keys not found") },
                }
            }
            None => { error!("DHT Key not found") },
        }
    }
    info!("Publishing route on DHT, done");


    // ------------------------------------------------------------
    info!("waiting for remote route");
    loop {
        let first_msg = receiver.recv()?;
        match first_msg {
            VeilidUpdate::AppMessage(ref msg) => {
                info!("Received message: {}", String::from_utf8(msg.message().to_vec())?.as_str());
            }
            VeilidUpdate::AppCall(call) => {

                info!("waiting for remote route, received");

                let route_id = api.import_remote_private_route(call.message().to_vec())?;

                info!("waiting for remote route, imported");

                let target = veilid_core::Target::PrivateRoute(route_id);

                let _ = api.app_call_reply(call.id(), b"SERVER ACK".to_vec()).await;

                let (receiver_clone, rc_clone, frames_clone) = (receiver.clone(), rc.clone(), frames.clone());
                tokio::spawn(async move { 
                    let _ = handle_request(target.clone(), receiver_clone, rc_clone, frames_clone).await;
                });
            }
            _ => (),
        };
    }
}

async fn handle_request(target: Target, _receiver: Receiver<VeilidUpdate>, rc: RoutingContext, frames: Vec<Value>) -> Result<(), Error> {

    let tx = async {
        info!("sending file");

        for frame in frames.iter() {
            // clear screen code
            let packet = "\x1B[2J\x1B[1;1H".to_owned() +
                // actual data
                frame.as_str().ok_or(anyhow!("Failed to decode frame as string"))?
                // newline
                + "\n";
            debug!("SENDING FRAME");
             rc.app_message(target.clone(), packet.as_bytes().to_vec())
            .await
            .context("app_message")?;
            tokio::time::sleep(core::time::Duration::from_millis(500)).await;
        }

        // send empty payload to finish
        rc.app_message(target.clone(), vec![])
            .await
            .context("app_message")?;
        info!("sending file, done");

        Ok::<_, Error>(())
    };

    tx.await?;
    Ok(())
}
