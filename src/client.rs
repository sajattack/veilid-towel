use anyhow::{Context, Error, anyhow};
use flume::{unbounded, Receiver, Sender};
use futures_util::select;
use tokio::io::AsyncWriteExt;
use tracing::info;
use veilid_core::{tools::*, Sequencing, CryptoKey};
use veilid_core::{api_startup, CryptoTyped, KeyPair, VeilidUpdate};
use base64::Engine;
use base64::engine::general_purpose;
use veilid_core::{SafetySpec, SafetySelection, Stability};


use crate::config::config_callback;
use crate::utils::{wait_for_attached, wait_for_network_start, wait_for_public_internet_ready};
use crate::CRYPTO_KIND;

pub async fn client(
    temp_dir: std::path::PathBuf,
    key_pair: CryptoTyped<KeyPair>,
    service_dht_key: CryptoTyped<CryptoKey>,
) -> Result<(), Error> {
    let mut stdout = tokio::io::stdout();

    let (sender, receiver): (
        Sender<veilid_core::VeilidUpdate>,
        Receiver<veilid_core::VeilidUpdate>,
    ) = unbounded();

    // Create VeilidCore setup
    let update_callback = Arc::new(move |change: veilid_core::VeilidUpdate| {
        if let Err(e) = sender.send(change) {
            // Don't log here, as that loops the update callback in some cases and will deadlock
            let change = e.into_inner();
            info!("error sending veilid update callback: {:?}", change);
        }
    });

    let temp_dir_clone = temp_dir.clone();
    let api = api_startup(
        update_callback,
        Arc::new(move |key| config_callback(temp_dir_clone.clone(), key_pair, key)),
    )
    .await
    .expect("startup failed");

    api.attach().await?;

    wait_for_network_start(&api).await;

    wait_for_attached(&api).await;

    let rc = api.routing_context()?.with_safety(
        SafetySelection::Safe(
            SafetySpec { 
                preferred_route: None,
                hop_count: 1,
                stability: Stability::Reliable,
                sequencing: Sequencing::EnsureOrdered,
            }
        )
    )?;

    wait_for_public_internet_ready(&api).await?;

    info!("Looking up route on DHT: {}", service_dht_key);
    let dht_desc = rc.open_dht_record(service_dht_key, None).await?;
    let dht_val = rc.get_dht_value(*dht_desc.key(), 1, true).await?.ok_or(anyhow!("DHT value not found"))?.data().to_vec(); 
    info!("DHT value: {}", String::from_utf8(dht_val.clone())?);
    rc.close_dht_record(*dht_desc.key()).await?;
    let their_route = general_purpose::STANDARD_NO_PAD.decode(String::from_utf8(dht_val)?)?;
    let their_route_id = api.import_remote_private_route(their_route)?;
    info!("Looking up route on DHT, done: {:?}", their_route_id);

    info!("creating a private route");
    let (_route_id, our_route) = api
        .new_custom_private_route(
            &[CRYPTO_KIND],
            veilid_core::Stability::Reliable,
            veilid_core::Sequencing::EnsureOrdered,
        )
        .await
        .context("new_custom_private_route")?;
    info!("creating a private route, done");

    let target = veilid_core::Target::PrivateRoute(their_route_id);

    let response = rc.app_call(target.clone(), our_route.clone())
        .await
        .context("app_call")?;

    info!("Received response: {}", String::from_utf8(response)?);

    tokio::time::sleep(core::time::Duration::from_millis(200)).await;

    let rx = async {
        loop {
            select! {
                res = receiver.recv_async() => {
                    if let Ok(change) = res {
                        if let VeilidUpdate::AppMessage(msg) = change {
                            let r = stdout.write(msg.message()).await?;
                            if r == 0 {
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }
            };
        }
        Ok::<_, Error>(())
    };

    //let (tx_done, rx_done) = tokio::join!(tx, rx);

    rx.await?;

    //api.shutdown().await;

    Ok(())
}
