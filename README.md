# veilid-towel: towel.blinkenlights.nl over Veilid

## Usage

### Server

#### Keygen

The first time you launch the server you need to generate some persistent keys so that your server
can be re-identified upon restarts and also for all that cryptographic goodness.
Keys are saved to ${CARGO_MANIFEST_DIR}/assets/secrets.json

`cargo run --release -- --server --new-keys`

#### Persistent Restart
The server can later be restarted without the --new-keys flag to maintain the same keys that were previously used
`cargo run --release -- --server`

The server will print some logs to stdout, including the DHT Key your private route is mapped to.

`INFO veilid_towel::server: DHT Key: VLD0:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`

This is what clients use to find your server.

The server has finished initialization once you see: `INFO veilid_towel::server: waiting for remote route`

### Client
By default, simply running the executable via `cargo run` or `./veilid-towel` will attempt to connect to my server's route.

To connect to other servers, specify their DHT key like so:
`cargo run --release -- --client VLD0:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`

## Acknowledgements
- The inspiration for this project and some portions of the code came from Bruno Bigras' [netdog project](https://gitlab.com/bbigras/netdog)
- I owe many thanks to the Veilid developers and the entire community for making this possible

## LICENSE
MIT


